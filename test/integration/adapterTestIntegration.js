/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-aviatrix',
      type: 'Aviatrix',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Aviatrix = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Aviatrix Adapter Test', () => {
  describe('Aviatrix Class Tests', () => {
    const a = new Aviatrix(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aviatrix-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aviatrix-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#genericGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.genericGet({ action: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#genericPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.genericPost({ action: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setAdminEmailAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setAdminEmailAddress('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericPost', 'action-setAdminEmailAddress', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#initialSetup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.initialSetup('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericPost', 'action-initialSetup', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureStorageAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAzureStorageAccounts('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=getAzureStorageAccounts', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVPCs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listVPCs(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listVPCs', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPublicSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPublicSubnets('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listPublicSubnets', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPrivateSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPrivateSubnets('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listPrivateSubnets', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTransitGatewaySupportedSizes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTransitGatewaySupportedSizes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listTransitGatewaySupportedSizes', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGateway(1, 'fakedata', 'fakedata', 'fakedata', null, 'fakedata', 'fakedata', 'fakedata', null, null, null, false, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=createTransitGateway', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTransitGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTransitGateways((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listTransitGateways', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableTransitHA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableTransitHA('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=enableTransitHA', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVGWs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listVGWs('fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listVGWs', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectTransitGatewayToVGW - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectTransitGatewayToVGW('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 1, false, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=connectTransitGatewayToVGW', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectTransitGatewayToAviatrixCloudN - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectTransitGatewayToAviatrixCloudN('fakedata', 'fakedata', 'fakedata', 1, 1, 'fakedata', 'fakedata', 1, null, null, null, null, null, null, null, null, 1, null, null, false, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=connectTransitGatewayToAviatrixCloudN', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVGWConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listVGWConnections((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listVGWConnections', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSpokeGatewaySupportedSizes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSpokeGatewaySupportedSizes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listSpokeGatewaySupportedSizes', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSpokeGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSpokeGateway('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', null, null, null, null, null, false, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=createSpokeGateway', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSpokeGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSpokeGateways((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listSpokeGateways', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableSpokeHA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableSpokeHA('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=enableSpokeHA', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableSpokeHAGCE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableSpokeHAGCE('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=enableSpokeHAGCE', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachSpokeToTransitGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.attachSpokeToTransitGateway('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=attachSpokeToTransitGateway', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachSpokeFromTransitGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.detachSpokeFromTransitGateway('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=detachSpokeFromTransitGateway', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableSpokeHA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableSpokeHA('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=disableSpokeHA', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSAMLInformation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSAMLInformation((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listSAMLInformation', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVPNGatewayConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVPNGatewayConfig('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=getVPNGatewayConfig', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomVPC - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomVPC('fakedata', 'fakedata', 'fakedata', 'fakedata', 1, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=createCustomVPC', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomVPCs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCustomVPCs(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listCustomVPCs', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomVPC - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCustomVPC('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=deleteCustomVPC', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnCloudNetworkInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnCloudNetworkInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=returnCloudNetworkInfo', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listResourceCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listResourceCounts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listResourceCounts', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPCTrackerOnDashboardMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.showVPCTrackerOnDashboardMap((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=showVPCTrackerOnDashboardMap', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hideVPCTrackerOnDashboardMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hideVPCTrackerOnDashboardMap((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=hideVPCTrackerOnDashboardMap', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disconnectActiveVPNuser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disconnectActiveVPNuser('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=disconnectActiveVPNuser', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setCustomerId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setCustomerId('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=setCustomerId', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCustomerId((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listCustomerId', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLicenseId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLicenseId((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listLicenseId', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAccounts('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=listAccounts', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAccount('fakedata', 'fakedata', 'fakedata', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=createAccount', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAccount('fakedata', 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=editAccount', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAccount('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=deleteAccount', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadFile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.return);
                assert.equal('string', data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Generic', 'genericGet', 'action=uploadFile', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#step2DownloadtheGatewaySyslog - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.step2DownloadtheGatewaySyslog('fakedata', (data, error) => {
            try {
              if (stub) {
                // const displayE = 'Error 400 received on request';
                // runErrorAsserts(data, error, 'AD.500', 'Test-aviatrix-connectorRest-handleEndResponse', displayE);
                runCommonAsserts(data, error);
                assert.equal(true, data.response.return);
                assert.equal(3, data.response.results.length);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DownloadGatewaySyslog', 'step2DownloadtheGatewaySyslog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
