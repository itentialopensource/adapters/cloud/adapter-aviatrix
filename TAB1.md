# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Aviatrix System. The API that was used to build the adapter for Aviatrix is usually available in the report directory of this adapter. The adapter utilizes the Aviatrix API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Aviatrix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Aviatrix. With this adapter you have the ability to perform operations such as:

- Automate the building of transit networks across multiple clouds.
- Get Gateway
- Extend to Edge Locations
- Create Transit Gateway
- Connect to Private Network

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
