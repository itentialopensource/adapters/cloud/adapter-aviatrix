# Aviatrix

Vendor: Aviatrix
Homepage: https://aviatrix.com/

Product: Aviatrix
Product Page: https://aviatrix.com/

## Introduction
We classify Aviatrix into the Cloud domain as Aviatrix provides the ability to manage and optimize networking and security operations in multi-cloud environments. 

"Easily visualize and manage the global cloud network, and troubleshoot connectivity issues." 
 
## Why Integrate
The Aviatrix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Aviatrix. With this adapter you have the ability to perform operations such as:

- Automate the building of transit networks across multiple clouds.
- Get Gateway
- Extend to Edge Locations
- Create Transit Gateway
- Connect to Private Network

## Additional Product Documentation
The [API documents for Aviatrix](https://support.aviatrix.com/apiDownloads)