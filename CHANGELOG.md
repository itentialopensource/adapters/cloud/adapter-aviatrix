
## 0.8.5 [10-15-2024]

* Changes made at 2024.10.14_20:54PM

See merge request itentialopensource/adapters/adapter-aviatrix!24

---

## 0.8.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aviatrix!22

---

## 0.8.3 [08-14-2024]

* Changes made at 2024.08.14_19:13PM

See merge request itentialopensource/adapters/adapter-aviatrix!21

---

## 0.8.2 [08-07-2024]

* Changes made at 2024.08.06_20:38PM

See merge request itentialopensource/adapters/adapter-aviatrix!20

---

## 0.8.1 [08-06-2024]

* Changes made at 2024.08.06_15:31PM

See merge request itentialopensource/adapters/adapter-aviatrix!19

---

## 0.8.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!18

---

## 0.7.5 [03-28-2024]

* Changes made at 2024.03.28_13:23PM

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!16

---

## 0.7.4 [03-11-2024]

* Changes made at 2024.03.11_15:36PM

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!15

---

## 0.7.3 [02-28-2024]

* Changes made at 2024.02.28_11:50AM

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!14

---

## 0.7.2 [12-24-2023]

* update metadata

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!13

---

## 0.7.1 [12-22-2023]

* update axios

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!12

---

## 0.7.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!11

---

## 0.6.0 [11-08-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!11

---

## 0.5.1 [05-23-2023]

* Fixed schema types

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!10

---

## 0.5.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!8

---

## 0.4.2 [03-01-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!7

---

## 0.4.1 [02-05-2021]

- Finish this migration (found some issues) also needed to fix repo references

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!6

---

## 0.4.0 [01-25-2021]

- Add 2 new calls attach and detach transit gateway from AWS TGW

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!5

---

## 0.3.4 [01-25-2021]

- Add CID to post calls in the adapter as it is needed for authentication and post calls need CID in the body instead of the URL.

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!4

---

## 0.3.3 [01-24-2021]

- Fixes for delete aviatrix gateway call - these may be temporary due to need to change CID

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!3

---

## 0.3.2 [01-24-2021] & 0.3.1 [01-24-2021]

- This fixes the dependency issues for winston and other items used by the scripts.

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!2

---

## 0.3.0 [12-18-2020] & 0.2.0 [12-17-2020]

- Add first set of calls

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!1

---

## 0.1.1 [09-11-2020]

- Initial Commit

See commit e24eaad

---
