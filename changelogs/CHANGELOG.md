
## 0.5.1 [05-23-2023]

* Fixed schema types

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!10

---

## 0.5.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!8

---

## 0.4.2 [03-01-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!7

---

## 0.4.1 [02-05-2021]

- Finish this migration (found some issues) also needed to fix repo references

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!6

---

## 0.4.0 [01-25-2021]

- Add 2 new calls attach and detach transit gateway from AWS TGW

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!5

---

## 0.3.4 [01-25-2021]

- Add CID to post calls in the adapter as it is needed for authentication and post calls need CID in the body instead of the URL.

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!4

---

## 0.3.3 [01-24-2021]

- Fixes for delete aviatrix gateway call - these may be temporary due to need to change CID

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!3

---

## 0.3.2 [01-24-2021] & 0.3.1 [01-24-2021]

- This fixes the dependency issues for winston and other items used by the scripts.

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!2

---

## 0.3.0 [12-18-2020] & 0.2.0 [12-17-2020]

- Add first set of calls

See merge request itentialopensource/adapters/cloud/adapter-aviatrix!1

---

## 0.1.1 [09-11-2020]

- Initial Commit

See commit e24eaad

---
