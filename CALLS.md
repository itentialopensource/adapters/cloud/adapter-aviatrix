## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Aviatrix. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Aviatrix.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>

  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Aviatrix. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">genericGet(query, callback)</td>
    <td style="padding:15px">Generic Get from Aviatrix</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericPost(body, callback)</td>
    <td style="padding:15px">Generic Post from Aviatrix</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAdminEmailAddress(adminEmail, callback)</td>
    <td style="padding:15px">Set Admin Email for Aviatrix controller.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">initialSetup(subaction, targetVersion, callback)</td>
    <td style="padding:15px">Run UCC(cloud Aviatrix controller) initial-setup if the value of 'subaction' is 'run'. Check if initial-setup has been run already or not.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureStorageAccounts(accountName, withImage, vpcReg, callback)</td>
    <td style="padding:15px">Get Azure Storage Accounts</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPCs(withVpn, withTransit, withSite2cloudConn, callback)</td>
    <td style="padding:15px">List all the VPCs which contain at least one Aviatrix gateway</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPublicSubnets(accountName, cloudType, region, vpcId, callback)</td>
    <td style="padding:15px">This API displays all the public subnets information (in the format of --> CIDRZoneName) with the given VPC-ID.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPrivateSubnets(accountName, cloudType, region, vpcId, callback)</td>
    <td style="padding:15px">List private subnets. Available only in UCC.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTransitGatewaySupportedSizes(callback)</td>
    <td style="padding:15px">lists all Transit-Gateway supported sizes</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitGateway(cloudType, accountName, region, vpcId, vnetAndResourceGroupNames, publicSubnet, gwName, gwSize, dnsServer, insaneMode, tags, reuseResourceGroup, allocateNewEip, eip, enableActivemesh, learnedCidrsApproval, callback)</td>
    <td style="padding:15px">List private subnets. Available only in UCC.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTransitGateways(callback)</td>
    <td style="padding:15px">lists transit gateways</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitHA(gwName, publicSubnet, allocateNewEip, eip, callback)</td>
    <td style="padding:15px">Enable transit HA</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVGWs(accountName, cloudType, region, callback)</td>
    <td style="padding:15px">List VGWs</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectTransitGatewayToVGW(connectionName, bgpVgwAccountName, bgpVgwRegion, vgwId, transitGw, vpcId, bgpLocalAsNumber, connectionPolicy, callback)</td>
    <td style="padding:15px">List private subnets. Available only in UCC.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectTransitGatewayToAviatrixCloudN(vpcId, connectionName, transitGw, bgpLocalAsNumber, cloudnAsNumber, cloudnIp, cloudnNeighborIp, cloudnNeighborAsNumber, enableHa, insaneMode, directConnect, backupInsaneMode, backupCloudnAsNumber, backupExternalDeviceAsNumber, backupDirectConnect, enableLoadBalancing, backupCloudnNeighborAsNumber, backupCloudnNeighborIp, backupCloudnIp, connectionPolicy, callback)</td>
    <td style="padding:15px">Setup BGP connection to Aviatrix CloudN</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVGWConnections(callback)</td>
    <td style="padding:15px">List VGW connections</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSpokeGatewaySupportedSizes(callback)</td>
    <td style="padding:15px">lists all spoke gateway supported sizes</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSpokeGateway(accountName, cloudType, region, vpcId, publicSubnet, gwName, vnetAndResourceGroupNames, gwSize, dnsServer, natEnabled, tags, insaneMode, skipRfc1918, reuseResourceGroup, allocateNewEip, eip, enableActivemesh, callback)</td>
    <td style="padding:15px">Creates a spoke gateway</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSpokeGateways(callback)</td>
    <td style="padding:15px">List spoke gateways</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSpokeHA(gwName, publicSubnet, newSubnet, callback)</td>
    <td style="padding:15px">Enable spoke HA</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSpokeHAGCE(gwName, newZone, callback)</td>
    <td style="padding:15px">Enable spoke HA (GCE)</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachSpokeToTransitGateway(spokeGw, transitGw, routeTableList, callback)</td>
    <td style="padding:15px">Attach spoke to transit gateway</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachSpokeFromTransitGateway(spokeGw, callback)</td>
    <td style="padding:15px">Detach spoke from transit gateway</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSpokeHA(gwName, callback)</td>
    <td style="padding:15px">Disable spoke HA</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSAMLInformation(callback)</td>
    <td style="padding:15px">List SAML information.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNGatewayConfig(vpcId, lborgatewayName, callback)</td>
    <td style="padding:15px">Get VPN gateway configuration.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomVPC(cloudType, poolName, accountName, vpcCidr, vpcCounts, aviatrixTransitVpc, publicSubnets, addGateway, region, vpcSize, securityUrl, subnetList, callback)</td>
    <td style="padding:15px">Create a new custom VPC</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomVPCs(poolNameOnly, callback)</td>
    <td style="padding:15px">List all custom VPCs in detail. Only valid for Userconnect</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomVPC(poolName, accountName, callback)</td>
    <td style="padding:15px">Delete a custom VPC. Only valid for Userconnect</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnCloudNetworkInfo(cache, callback)</td>
    <td style="padding:15px">Return cloud network info</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceCounts(callback)</td>
    <td style="padding:15px">Return a list of resource counts and health status</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVPCTrackerOnDashboardMap(callback)</td>
    <td style="padding:15px">Show VPC tracker on dashboard page map</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hideVPCTrackerOnDashboardMap(callback)</td>
    <td style="padding:15px">Hide VPC tracker on dashboard page map</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectActiveVPNuser(username, gatewayName, remoteIp, remotePort, callback)</td>
    <td style="padding:15px">Disconnets an active VPN user</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setCustomerId(customerId, callback)</td>
    <td style="padding:15px">Set Aviatrix Customer ID.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerId(callback)</td>
    <td style="padding:15px">List Aviatrix Customer IDs.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLicenseId(callback)</td>
    <td style="padding:15px">List Aviatrix License IDs.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccounts(awsIamRoleBased, callback)</td>
    <td style="padding:15px">List all the accounts</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccount(accountName, cloudType, accountEmail, accountPassword, awsAccountNumber, awsIam, awsAccessKey, awsSecretKey, awsRoleArn, awsRoleEc2, azureSubscriptionId, gcloudProjectName, gcloudProjectCredentials, armSubscriptionId, armApplicationEndpoint, armApplicationClientId, armApplicationClientSecret, awsgovAccountNumber, awsgovAccessKey, awsgovSecretKey, awsgovCloudtrailBucket, azurechinaSubscriptionId, awschinaAccountNumber, awschinaAccessKey, awschinaSecretKey, armChinaSubscriptionId, armChinaApplicationEndpoint, armChinaApplicationClientId, armChinaApplicationClientSecret, ociUserId, ociTenancyId, ociCompartmentId, ociApiKey, callback)</td>
    <td style="padding:15px">Create an account.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAccount(accountName, cloudType, awsAccountNumber, awsIam, awsAccessKey, awsSecretKey, callback)</td>
    <td style="padding:15px">Edit an account.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccount(accountName, callback)</td>
    <td style="padding:15px">Delete an account.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(filename, contents, callback)</td>
    <td style="padding:15px">Upload File</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadtheGatewaySyslog(filename, callback)</td>
    <td style="padding:15px">Step 2: Download the Gateway Syslog</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logIntoControllerGetCID(username, callback)</td>
    <td style="padding:15px">Description
-------------

By invoking this API, user is able to acquire "CID/Session ID"/token" wh</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forgetControllerPassword(username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Forget Controller Password.
+ This API is available in version 3.2 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudNetworkInfo(cache, callback)</td>
    <td style="padding:15px">Description
-------------

+ Return cloud network info
+ This API is available in version 3.4 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectActiveVPNUsers(username, gatewayName, remoteIp, remotePort, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disconnets an active VPN user.
+ This API is available in version 5.1</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAviatrixCustomerID(customerId, callback)</td>
    <td style="padding:15px">Description
-------------

Set Aviatrix Customer ID.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerIDs(callback)</td>
    <td style="padding:15px">Description
-------------

List Aviatrix Customer IDs.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLicenseIDs(callback)</td>
    <td style="padding:15px">Description
-------------

List Aviatrix License IDs.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccountGCloud(accountName, gcloudProjectName, gcloudProjectCredentials, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create an account. This has to be done by an admin. This account can b</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadGCloudProjectCredentialFileToController(filename, contents, callback)</td>
    <td style="padding:15px">Description
-------------

+ Upload a GCloud project credential file to the Aviatrix Controller. Th</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccountARM(accountName, accountEmail, armSubscriptionId, armApplicationEndpoint, armApplicationClientId, armApplicationClientSecret, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create an account. This has to be done by an admin. This account can b</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccountOCI(accountName, accountEmail, ociUserId, ociTenancyId, ociCompartmentId, ociApiKey, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create an Oracle OCI account. This has to be done by an admin. This ac</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccountAWSAccessKeyBased(accountName, accountEmail, awsAccountNumber, awsIam, awsAccessKey, awsSecretKey, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create an account. This has to be done by an admin. This account can b</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccountAWSIAMRoleBased(accountName, accountEmail, awsAccountNumber, awsIam, awsRoleArn, awsRoleEc2, awsAccessKey, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create an account. This has to be done by an admin. This account can b</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccountUsers(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the account users.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccountUser(accountName, username, password, email, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a new account user
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAccountUser(accountName, username, password, what, email, oldPassword, newPassword, callback)</td>
    <td style="padding:15px">Description
-------------

+ Change account user's account name, email, or password</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountUser(username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete an account user.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountAuditConfigiuration(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API returns the backround account audit control configuration, su</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAccountBackgroundAudit(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable the access account background audit
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAccountBackgroundAudit(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable the access account background audit
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccountAudit(accessAccountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update an Access Account IAM policy to the default one
+ This API is a</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAccountBackgroundAuditEmailNotification(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable the access account background audit Email Notification
+ This A</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAccountBackgroundAuditEmailNotification(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable the access account background audit Email Notification
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkAccountAudit(accessAccountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Checks account audit for specified account.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPeeringHAGateway(publicSubnet, gwName, newZone, eip, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create peering HA gateway.
+ This API is available in version 3.2 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gatewayHASwitchOver(vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Force a gateway with HA enabled to switch over to the backup gateway
+</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSingleAZHA(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable single AZ HA
+ This API is available in version 3.1 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSingleAZHA(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable Single AZ HA
+ This API is available in version 3.1 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceTags(resourceType, resourceName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List resource tags.
+ This API is available in version 3.1 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResourceTags(resourceType, resourceName, newTagList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add resource tags.
+ This API is available in version 3.1 or later rel</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourceTags(resourceType, resourceName, newTagList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update resource tags.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceTags(resourceType, resourceName, delTagList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete resource tags.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeGatewaySize(gwName, gwSize, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API changes the specified gateway's size.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSNATSupportedProtocols(callback)</td>
    <td style="padding:15px">Description
-------------

+ List SNAT supported protocols.
+ This API is available in version 3.4</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSNATSupportedInterfaces(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List SNAT supported interfaces.
+ This API is available in version 3.4</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSNAT(gatewayName, mode, policyList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable SNAT.
+ This API is available in version 3.3 or later releases.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSNAT(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable SNAT.
+ This API is available in version 3.3 or later releases.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayDNATConfiguration(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get gateway DNAT configuration
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNATSupportedInterfaces(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List DNAT supported interfaces.
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNATSupportedProtocols(action, cID, callback)</td>
    <td style="padding:15px">Description
-------------

+ List DNAT supported protocols
+ This API is available in version 3.3 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNATConfiguration(gatewayName, policyList0DstIp, policyList0NewDstIp, policyList0Protocol, policyList0Port, policyList0Interface, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update DNAT configuration.
+ This API is available in version 3.3 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewaySNATConfiguration(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get gateway SNAT configuration.
+ This API is available in version 3.4</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableMonitorGatewaySubnets(gatewayName, monitorExcludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable monitor gateway subnets.
+ This API is available in version 3.2</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableMonitorGatewaySubnets(gatewayName, monitorExcludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable monitor gateway subnets.
+ This API is available in version 3.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDesignatedGatewayAdditionalCIDRList(gatewayName, additionalCidrList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set designated gateway additional CIDR List
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSKIPRFC1918Routes(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable SKIP_RFC1918 Routes
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editGatewayCustomRoutes(gatewayName, cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Gateway Custom routes.
+ This API is available in version 4.2 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editGatewayFilterRoutes(gatewayName, cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Gateway filter routes
+ This API is available in version 4.2 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configVPCCIDRForAdvertisedToOnprem(gatewayName, cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Config vpc cidr that will be included for advertised to onprem.
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitPeeringForOnpremBackup(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable the feature of using transit peering for onprem backup.
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitPeeringForOnpremBackup(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable the feature of using transit peering for onprem backup.
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVPCDNSServer(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable VPC DNS server for existing gateway
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableVPCDNSServer(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable VPC DNS Server for an existing gateway.
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptGatewayVolume(gatewayName, customerManagedKeys, callback)</td>
    <td style="padding:15px">Description
-------------

+ Encrypt Gateway Volume
+ This API is available in version 4.2 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGatewayHA(vpcName, specificSubnet, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable Gateway HA
+ This API is available in version 3.0 or later rele</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGatewayHA(vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable gateway HA.
+ This API is available in version 3.0 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableActiveMesh(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable activemesh mode in gateway
+ This API is available in version 5</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableActiveMesh(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable activemesh mode in gateway
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVPNGatewayVirtualAddressRange(gatewayName, vpnCidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edits the virtual address range of a single VPN gateway.
+ This API is</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableIPv6ForAGateway(ipv6Action, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable IPv6 for a Gateway
+ This API is available in version 5.0 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableIPv6ForAGateway(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable IPv6 for a Gateway
+ This API is available in version 5.0 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSecondaryIPForAGateway(gatewayName, editSecondaryIp, callback)</td>
    <td style="padding:15px">Description
-------------

+ Sets secondary IP for a specified gateway.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">repairGCEGatewaySSHKey(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Repairs corrputed GCE gateway SSH keys.
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGatewayNetworkMappingConfiguration(gatewayName, policyList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Updates gateway network mapping configuration.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGatewayPeriodicPing(gatewayName, interval, ipAddress, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enables periodic ping on a gateway where customers can specify an IP a</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGatewayPeriodicPing(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disables periodic ping on a gateway where customers can specify an IP</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayPeriodicPingStatus(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets a gateway's periodic ping status.
+ This API is available in vers</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportedGatewaySizes(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all of supported gateway sizes for each cloud type.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportedRegions(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all of the supported regions for each cloud type</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUnassociatedEIPs(accountName, region, callback)</td>
    <td style="padding:15px">Description
-------------

+ List unassociated elastic IPs.
+ This API is available in version 3.5</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAviatrixGatewayAWS(accountName, vpcReg, vpcId, vpcNet, gwName, vpcSize, enableNat, designatedGateway, allocateNewEip, eip, insaneMode, zone, tags, vpnAccess, samlEnabled, otpMode, saveTemplate, cIDR, reuseResourceGroup, duoIntegrationKey, duoSecretKey, duoApiHostname, duoPushMode, oktaUrl, oktaToken, oktaUsernameSuffix, enableElb, elbName, elbProtocol, enableClientCertSharing, maxConn, splitTunnel, additionalCidrs, nameservers, searchDomains, enablePbr, pbrSubnet, pbrDefaultGateway, pbrLogging, enableLdap, ldapServer, ldapBindDn, ldapPassword, ldapBaseDn, ldapUserAttr, ldapAdditionalReq, ldapUseSsl, ldapClientCert, ldapCaCert, callback)</td>
    <td style="padding:15px">Description
-------------

Create Aviatrix gateway in AWS.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAviatrixGatewayGCloud(accountName, vpcId, vpcNet, gwName, vpcSize, enableNat, zone, vpnAccess, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

Create Aviatrix gateway in GCloud.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAviatrixGatewayAzure(accountName, vpcReg, vpcId, vpcNet, gwName, vpcSize, enableNat, designatedGateway, vpnAccess, saveTemplate, reuseResourceGroup, callback)</td>
    <td style="padding:15px">Description
-------------

Create Aviatrix gateway in Azure.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAviatrixGatewayOCI(accountName, vpcReg, vpcId, vpcNet, gwName, vpcSize, enableNat, callback)</td>
    <td style="padding:15px">Description
-------------

Create Aviatrix gateway in Azure.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVPNGatewayWithELBDUOMFAPBR(accountName, gwName, vpcReg, vpcId, vpcNet, vpcSize, enableNat, designatedGateway, allocateNewEip, vpnAccess, cidr, maxConn, splitTunnel, additionalCidrs, searchDomains, enableElb, elbName, otpMode, duoIntegrationKey, duoSecretKey, duoApiHostname, duoPushMode, enablePbr, pbrSubnet, pbrDefaultGateway, pbrLogging, saveTemplate, tags, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create a VPN gateway.
+ This API is available in version 1.0 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSAMLVPNGatewayAWS(accountName, gwName, vpcReg, vpcId, vpcNet, vpcSize, allocateNewEip, enableNat, vpnAccess, cidr, maxConn, splitTunnel, samlEnabled, enableElb, saveTemplate, tags, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create a SAML VPN Gateway. 
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAviatrixGateway(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

Delete Aviatrix Gateway.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayInformation(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get gateway information.
+ This API is available in version 3.3 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewaysInformation(accountName, acxGwOnly, callback)</td>
    <td style="padding:15px">Description
-------------

By invoking this API, user is able to retrieve detail information of one</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGCEZoneBySubnet(subnet, accountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get List GCE Zone by subnet
+ This API is available in version 4.2 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstances(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists instances and instance details.
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetNativeVPNConfigurationFile(tgwName, vpnConnectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the native VPN configuration file
+ To download the configuration</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadTheNativeVPNConfiguration(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAWSTGW(accountName, region, tgwName, awsSideAsn, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a new AWS TGW
+ This API is available in version 4.0 or later rele</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRouteDomain(tgwName, routeDomainName, firewallDomain, nativeEgressDomain, nativeFirewallDomain, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a new route domain
+ This API is available in version 4.0 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAviatrixDefaultRouteDomainName(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get Aviatrix default route domain name.
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConnectionBetweenRouteDomains(tgwName, sourceRouteDomainName, destinationRouteDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a connection between 2 route domains
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectionBetweenRouteDomains(tgwName, sourceRouteDomainName, destinationRouteDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete Connection between 2 route domains
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachNativeVPNToTGW(tgwName, routeDomainName, connectionName, publicIp, onpremAsn, remoteCidr, insideIpCidrTun1, preSharedKeyTun1, insideIpCidrTun2, preSharedKeyTun2, learnedCidrsApproval, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach native VPN to TGW
+ This API is available in version 4.6 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachVPNFromTGW(tgwName, vpnId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach VPN from TGW
+ This API is available in version 4.6 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachNativeDirectconnectToTGW(tgwName, routeDomainName, directconnectAccountName, directconnectGatewayId, allowedPrefix, learnedCidrsApproval, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach native Directconnect to TGW
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachDirectconnectFromTGW(tgwName, directconnectId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach Directconnect from TGW
+ This API is available in version 4.6 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitGatewayInterfaceToAWSTGW(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable transit gateway interface to AWS TGW
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitGatewayInterfaceToAWSTGW(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable transit gateway interface to AWS TGW
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExistingDirectconnectAllowedPrefixList(tgwName, directconnectGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the existing directconnect allowed prefix list.
+ This API is avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteDomain(tgwName, routeDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete an existing route domain
+ This API is available in version 4.0</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAWSTGW(tgwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete an existing AWS TGW.
+ This API is available in version 4.0 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAWSTGWPeering(tgwName1, tgwName2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create an AWS native inter-region TGW peering between two TGWs in two</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAWSTGWPeering(tgwName1, tgwName2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Deletes an existing AWS inter-region TGW peering
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPeeredTGWNames(tgwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists the names of the TGWs peered to the specified TGW.
+ This API is</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachVPCToAWSTGW(region, vpcAccountName, vpcName, tgwName, routeDomainName, subnetList, routeTableList, customizedRoutes, customizedRouteAdvertisement, disableLocalRoutePropagation, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach VPC to AWS TGW
+ This API is available in version 4.0 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachVPCFromAWSTGW(tgwName, routeDomainName, vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach VPC from AWS TGW
+ This API is available in version 4.0 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectVPCsViaTGW(tgwName, accountName1, vpcId1, accountName2, vpcId2, subnetList1, subnetList2, routeTableList1, routeTableList2, customizedRoutes1, customizedRoutes2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Automatically connect two VPCs via a TGW.  Tasks include create TGW ro</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectVPCsViaTGW(tgwName, vpcId1, vpcId2, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API disconnects two connected VPCs via TGW.  Tasks include remove</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTGWSpokeVPCCustomizedRoutes(tgwName, vpcId, routeList, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API edits the customized spoke routes to an attached VPC attachme</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTGWNames(region, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the TGW names.
+ This API is available in version 4.0 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRouteDomainNames(tgwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the route domain names.
+ This API is available in version 4.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConnectedRouteDomainNames(tgwName, routeDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all other route domain names connected to a route domain
+ This A</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPCNamesAttachedToRouteDomain(tgwName, routeDomainName, resourceType, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the VPC names that attached to a route domain
+ This API is a</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPCIDsWithNameTag(vpcId, accountName, vpcRegion, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the VPC IDs with Name tag
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllTGWAttachments(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all attachment detail information. Each entry represents a VPC at</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTGWDetails(tgwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List TGW details.
+ This API is available in version 4.1 or later rele</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachmentRouteTableDetails(tgwName, attachmentName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List the detail route table information associated with a VPC attachme</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDirectconnectGateways(accountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List Directconnect Gateways
+ This API is available in version 4.6 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTGWWithCloudType(callback)</td>
    <td style="padding:15px">Description
-------------

+ List TGW with cloud type.
+ This API is available in version 4.6 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVPCCustomizedRouteAdvertisement(tgwName, attachmentName, cidrList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Updates a VPC customized route advertisement to a TGW.
+ This API is a</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVPNRemoteCIDR(tgwName, attachmentName, cidrList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Updates a static VPN attachment's remote CIDRs.
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTGWPerVPCAudit(tgwName, vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Run a per VPC TGW route audit
+ This API is available in version 5.2 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTGWSpokeVPCCIDRS(tgwName, vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API updates the VPC's CIDR(s) by updating or propagating routes i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDirectconnectAllowedPrefixList(tgwName, directconnectGatewayName, allowedPrefix, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update the directconnect allowed prefix list.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTGW3DViewData(tgwName, attachmentName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get TGW 3D view data
+ This API is available in version 4.0 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewRouteDomainDetails(tgwName, routeDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ View the details of a route domain
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTGWNightlyAutomaticAuditStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get TGW audit state
+ This API is available in version 4.1 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTGWNightlyAutomaticAudit(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable TGW audit
+ This API is available in version 4.1 or later relea</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTGWNightlyAutomaticAudit(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable TGW audit
+ This API is available in version 4.1 or later rele</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTGWOnDemandAudit(tgwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Run TGW On-demand Audit
+ This API is available in version 4.1 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTGWAttachmentDetails(tgwName, attachmentName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets a TGW attachment's details.
+ This API is available in version 5.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPendingAndApprovedCIDRsLists(tgwName, attachmentName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get TGW VPN/DirectGateway attachment's pending/approved learned CIDRss</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableLearnedCIDRsApproval(tgwName, attachmentName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable TGW VPN/DirectGateway attachment's learned cidrs approval featu</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableLearnedCIDRsApproval(tgwName, attachmentName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable TGW VPN/DirectGateway attachment's learned cidrs approval feat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePendingAndApprovedCIDRsLists(tgwName, attachmentName, pendingLearnedCidrs, approvedLearnedCidrs, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update a TGW VPN/DirectGateway attachment's pending/approved learned c</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectTransitGatewayToExternalDevice(vpcId, connectionName, transitGw, routingProtocol, bgpLocalAsNumber, remoteSubnet, externalDeviceAsNumber, externalDeviceIpAddress, directConnect, preSharedKey, localTunnelIp, remoteTunnelIp, phase1Encryption, phase2Encryption, phase1DhGroups, phase2DhGroups, phase1Authentication, phase2Authentication, enableHa, backupExternalDeviceIpAddress, backupExternalDeviceAsNumber, backupDirectConnect, backupPreSharedKey, backupLocalTunnelIp, backupRemoteTunnelIp, connectionPolicy, callback)</td>
    <td style="padding:15px">Description
-------------

+ Setup BGP connection to external device.
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectTransitGatewayFromVGW(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disconnect a transit gateway from a VGW.
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachARMNativeSpokeToTransitGateway(transitGatewayName, accountName, region, vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach ARM Native Spoke to Transit Gateway
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachARMNativeSpokeToTransitGateway(transitGatewayName, spokeName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach ARM Native Spoke to Transit Gateway
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listARMNativeSpokes(transitGatewayName, details, callback)</td>
    <td style="padding:15px">Description
-------------

+ List ARM Native Spokes.
+ This API is available in version 5.0 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAviatrixTransitGateways(callback)</td>
    <td style="padding:15px">Description
-------------

+ Displays information on List page for all transit network related tran</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPrimaryAndHASpokeGateways(callback)</td>
    <td style="padding:15px">Description
-------------

+ Display information on List page for all primary or HA spoke gateways
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAviatrixTransitGatewaysDisplayFields(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists Aviatrix transit gateways display fields
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSpokeGatewayDisplayFields(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists spoke gateways display fields
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitOrSpokeGatewayDetails(gatewayName, option, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets gateway details for specified transit or spoke gateway
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInterTransitGatewayPeering(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all existing inter transit gateway peering pairs, e.g. A is peeri</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterTransitGatewayPeering(gateway1, gateway2, srcFilterCidrs, dstFilterCidrs, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create inter transit gateway peering between two Aviatrix transit gate</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editInterTransitGatewayPeering(gateway1, gateway2, srcFilterCidrs, dstFilterCidrs, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit inter transit gateway peering between two Aviatrix transit gatewa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterTransitGatewayPeering(gateway1, gateway2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete inter transit gateway peering between two Aviatrix transit gate</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterTransitGatewayPeeringDetails(gateway1, gateway2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show info of one inter transit gateway peering pair.
+ This API is ava</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showInterTransitGatewayPeeringParameters(gateway1, gateway2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show inter transit gateway peering parameters.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitOrSpokeGatewayDetailParameters(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets transit or spoke gateway detail parameters
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGatewayForTransitFirenetFunction(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable gateway Transit Firenet function
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGatewayForTransitFirenetFunction(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable gateway Transit Firenet function
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTransitFirenetSpokePolicies(callback)</td>
    <td style="padding:15px">Description
-------------

+ List Transit Firenet spoke inspection policies
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSpokeToTransitFirenetInspection(firenetGatewayName, spokeGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add spoke to Transit Firenet inspecting set
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTransitFirenetManagementAccess(gatewayName, managementAccess, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Transit Firenet management access property
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSpokeFromTransitFirenetInspection(firenetGatewayName, spokeGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete spoke from Transit Firenet inpsecting set
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableConnectedTransitOnGateway(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable connected transit mode on transit gateway.
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableConnectedTransitOnGateway(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable connected transit mode on a transit gateway.
+ This API is ava</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAdvertiseTransitVPCNetworkCIDR(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable advertise transit VPC network CIDR for a Site2Cloud connection.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAdvertiseTransitVPCNetworkCIDR(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable advertise transit VPC network CIDR for a site2cloud connection</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeBGPManualSpokeAdvertisement(subaction, gatewayName, bgpManualSpokeAdvertiseCidrs, callback)</td>
    <td style="padding:15px">Description
-------------

+ Change BGP manual spoke advertisement CIDRs
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">prependASPath(subaction, gatewayName, bgpPrependAsPath, callback)</td>
    <td style="padding:15px">Description
-------------

+ Prepend AS Path
+ This API is available in version 5.1 or later releas</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshBGPAdvertiseNetworks(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Refresh BGP advertise networks to reflect the AS Path info introduced</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRoutesToSpokeVPC(gatewayName, cidrList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add routes to spoke VPC.
+ This API is available in version 4.3 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoutesFromSpokeVPC(gatewayName, cidrList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete routes from spoke VPC
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitEdgeSegmentation(gatewayName, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enables Edge Segmentation on selected site2cloud connection
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitEdgeSegmentation(gatewayName, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disables Edge Segmentation on seleced site2cloud connection
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBGPGateways(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all BGP gateways.
+ This API is available in version 3.0.1 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableBGPDampening(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable BGP dampening.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableBGPDampening(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable BGP Dampening.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBGPSite2CloudConnections(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the BGP enabled Site2Cloud connections.
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBGPLearnedRoutes(vpcId, connName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show BGP learned routes.
+ This API is available in version 3.1 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBGPAdvertisedNetworks(vpcId, connName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show BGP advertised networks.
+ This API is available in version 3.1 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBGPManualSpokeAdvertisedNetworks(vpcId, connectionName, cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set BGP manual spoke advertised networks.
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableBGPManualSpokeAdvertisedNetworks(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable BGP manual spoke advertised networks.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBGPDiagnosticCommands(gatewayName, egressInterfaceId, firewallName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List BGP diagnostic commands.
+ This API is available in version 3.2 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runBGPDiagnostic(gatewayName, command, callback)</td>
    <td style="padding:15px">Description
-------------

+ Run BGP diagnostic.
+ This API is available in version 3.2 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableBGPOverlappingAlertEmail(gatewayName, additionalCidrList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable BGP overlapping alert email.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPOverlappingAlertEmailStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get BGP overlapping alert email status.
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableBGPOverlappingAlertEmail(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable BGP overlapping alert email.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableBGPRouteLimitationEmail(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable BGP route limitation alert email.
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPRouteLimitationAlertEmailStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get BGP route limitation alert email status.
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableBGPRouteLimitationAlertEmail(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable BGP route limitation alert email.
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTransitGatewayAdvancedConfigDetails(transitGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Shows all transit gateways to be listed on "Transit Network" --> "Adva</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitLearnedCIDRsApproval(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable a transit gateway's learned cidrs approval feature
+ This API i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitLearnedCIDRsApproval(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable a transit gateway's learned cidrs approval feature
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1ListFireNets(subaction, vpcId, firewallInstance, detail, private_, gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List FireNets according to parameters
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadTheFirewallInstancePemFile(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitGatewayFireNetInterfaces(gatewayName, main, mainHa, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable transit gateway FireNet interfaces.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFireNets(subaction, vpcId, firewallInstance, detail, private_, gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List FireNets according to parameters
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallImages(callback)</td>
    <td style="padding:15px">Description
-------------

+ List firewall images.
+ This API is available in version 4.2 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallInstanceInformationByFirewallID(instanceId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get Firewall Instance By ID.
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchAndAssociateFirewallInstanceWithFireNet(gwName, firewallName, firewallImage, firewallSize, egressSubnet, managementSubnet, keyName, attach, iamRole, bootstrapBucketName, noAssociate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add firewall instance.
+ This API is available in version 4.3 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFireNetFirewallInstance(vpcId, firewallId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete FireNet firewall instance.
+ This API is available in version 4</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateFirewallToFireNet(vpcId, gatewayName, firewallId, lanInterface, egressInterface, managementInterface, firewallName, attach, callback)</td>
    <td style="padding:15px">Description
-------------

+ Insert firewall to FireNet.
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateFirewallFromFireNet(vpcId, firewallId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Remove firewall from FireNet
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectFireNetToTGW(vpcId, tgwName, domainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Connect FireNet to TGW
+ This API is available in version 4.3 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disconnectFireNetWithTGW(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disconnect FireNet with TGW
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchAndAssociateFQDNGatewayWithFireNet(gatewayName, fqdnGatewayName, fqdnGatewaySubnet, fqdnGatewaySize, attach, callback)</td>
    <td style="padding:15px">Description
-------------

+ Launch and associate an Aviatrix FDQN gateway as a firewall instance i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitGatewayFireNetInterfaces(gateway, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable transit gateway FireNet interfaces.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDetailInformationOfFireNet(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show detail information of FireNet.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceSwitchoverTransitGatewayInFirewallNetwork(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Force switchover transit gateway in firewall network.
+ This API is av</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDisableFirewallEgressAndInspection(vpcId, inspection, firewallEgress, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable/disable firewall egress and inspection.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachFirewallToFireNet(vpcId, firewallId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach Firewall to FireNet.
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachFirewallFromFireNet(vpcId, firewallId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach Firewall from FireNet.
+ This API is available in version 4.3 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFirewallVendorIntegrationInfo(vpcId, firewallId, firewallName, firewallVendor, user, password, publicIp, routeTable, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Firewall Vendor Integration Info.
+ This API is available in vers</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAndSyncFirewallVendorConfigurations(vpcId, firewallId, sync, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show and sync firewall vendor configurations.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchFirewallInstance(accountName, region, vpcId, instanceName, firewallImage, managementSubnet, egressSubnet, mainInterfaceSubnet, companionInterfaceSubnet, callback)</td>
    <td style="padding:15px">Description
-------------

+ Launch firewall instance.
+ This API is available in version 4.2 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallInstances(callback)</td>
    <td style="padding:15px">Description
-------------

+ List firewall instances.
+ This API is available in version 4.2 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallInstance(vpcId, instanceId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete Firewall Instance.
+ This API is available in version 4.2 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">prepareTransitGatewayForDMZ(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable gateway DMZ interface.
+ This API is available in version 4.1 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstancesAndInterfacesInVPC(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ List instances and interfaces in DMZ.
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitDMZ(vpcId, mainGatewayName, companionGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable VPC as DMZ network.
+ This API is available in version 4.1 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitDMZ(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable VPC as DMZ network.
+ This API is available in version 4.1 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertFirewallToDMZ(vpcId, firewallId, mainInterfaceId, companionInterfaceId, egressInterfaceId, managementInterfaceId, firewallName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Insert firewall instance to DMZ.
+ This API is available in version 4.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDMZVPCs(callback)</td>
    <td style="padding:15px">Description
-------------

+ List DMZ networks.
+ This API is available in version 4.1 or later rel</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDMZFirewalls(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ List DMZ firewalls.
+ This API is available in version 4.1 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeFirewallFromDMZ(vpcId, firewallId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Remove firewall instance from DMZ
+ This API is available in version 4</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTransitGatewayForDMZ(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable gateway DMZ interface
+ This API is available in version 4.1 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1ListFirewallInstances(callback)</td>
    <td style="padding:15px">Description
-------------

+ List firewall instances.
+ To download the pem file, copy the file nam</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDMZFirewallConfiguration(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show DMZ firewall configuration.
+ This API is available in version 4.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDMZFirewallConfiguration(vpcId, firewallInstanceId, firewallName, firewallVendor, firewallUserId, firewallPassword, publicIp, routeTable, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit DMZ firewall configuration
+ This API is available in version 4.1</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchoverDMZGateway(vpcId, gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Force switchover DMZ gateway.
+ This API is available in version 4.1 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeDMZFirewall(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Restore DMZ firewall.
+ This API is available in version 4.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bypassDMZFirewall(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Bypass DMZ firewall.
+ This API is available in version 4.1 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchoverDMZFirewall(vpcId, firewallId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Force switchover DMZ firewall.
+ This API is available in version 4.1</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDMZFirewallEgress(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable DMZ firewall egress.
+ This API is available in version 4.1 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDMZFirewallEgress(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable DMZ firewall egress.
+ This API is available in version 4.1 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDMZEastWestTrafficInspection(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable DMZ East West traffic inspection.
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDMZEastWestTrafficInspection(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable DMZ east west traffic inspection
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDMZInfo(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get DMZ info for a specific DMZ.
+ This API is available in version 4.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVendorFirewallInfo(vpcId, firewallInstanceId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show vendor firewall internal info.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerABranch(branchName, publicIp, hostOs, username, privateKeyFile, password, port, addr1, addr2, city, state, country, zipcode, description, callback)</td>
    <td style="padding:15px">Description
-------------

+ Register a Cloud WAN branch device
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBranchesToDeregister(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all Cloud WAN branch device names that are ready to be deregister</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deregisterABranch(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Deregister a Cloud WAN branch device
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBranchInformation(branchName, publicIp, hostOs, username, privateKeyFile, password, port, addr1, addr2, city, state, country, zipcode, description, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update a Cloud WAN branch device info
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveABranchSCurrentConfiguration(branchName, cfgName, description, callback)</td>
    <td style="padding:15px">Description
-------------

+ Archive a Cloud WAN branch device configuration
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSavedConfigurationsSummary(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all saved configurations and details for a specified branch.
+ T</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllSavedConfigurationNames(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all saved configuration names for a specifed branch.
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedConfigurationDetails(branchName, cfgName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the details of a specifc saved configuration.
+ This API is avail</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedShowRunConfiguration(branchName, cfgName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the configuraiton of the specifed saved configuration name
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreASavedConfiguration(branchName, cfgName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Restore a branch to a saved configuration.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBranchDevicesSummary(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all Cloud WAN branch devices summary
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBranchDeviceDetails(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get details of a Cloud WAN branch device
+ This API is available in ve</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentShowRunConfigurationOfABranch(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the current configuration of a Cloud WAN branch device
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runShowCommandOnABranchDevice(branchName, customInput, callback)</td>
    <td style="padding:15px">Description
-------------

+ Execute show commands in a Cloud WAN branch device
+ This API is avail</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editBranchDeviceConfiguration(branchName, customCfg, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit a Cloud WAN branch device configuration
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rollbackABranchToPreviousCommitConfiguration(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Rollback a Cloud WAN branch device configuration
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRegisteredBranchesNotAttached(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all registered Cloud WAN branch device names that are not yet att</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBranchWANInterfaces(branchName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get available WAN interfaces on a Cloud WAN branch device
+ This API i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureBranchWANInterfaces(branchName, wanPrimaryIf, wanPrimaryIp, wanBackupIf, wanBackupIp, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select WAN Primary/Backup interface(s) in a Cloud WAN branch device
+</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBranchesToAttach(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all Cloud WAN branch device names that are ready to be attached
+</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachBranchToAviatrixTransitGateway(connectionName, transitGw, branchName, routingProtocol, bgpLocalAsNumber, externalDeviceAsNumber, preSharedKey, localTunnelIp, remoteTunnelIp, phase1Encryption, phase2Encryption, phase1DhGroups, phase2DhGroups, phase1Authentication, phase2Authentication, enableHa, backupPreSharedKey, backupLocalTunnelIp, backupRemoteTunnelIp, enableGlobalAccelerator, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach a Cloud WAN branch device to Aviatrix Transit Gateway
+ This AP</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachBranchToAWSTGW(tgwName, branchName, routeDomainName, connectionName, externalDeviceAsNumber, enableGlobalAccelerator, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach a Cloud WAN branch device to AWS Transit Gateway
+ This API is</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudWANAttachments(callback)</td>
    <td style="padding:15px">Description
-------------

+ Show all Cloud WAN branch attachments
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachBranchFromGateway(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Deatch a Cloud WAN branch from Aviatrix Transit Gateway or AWS Transit</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAGlobalNetwork(accountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Creates a new global network
+ This API is available in version 5.2 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAWSGlobalNetworks(accountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all global networks
+ This API is available in version 5.2 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerTGWWithGlobalNetwork(accountName, tgwName, globalNetwork, callback)</td>
    <td style="padding:15px">Description
-------------

+ Registers an AWS Transit Gateway with a global network
+ This API is a</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerBranchWithGlobalNetwork(accountName, branchName, globalNetwork, callback)</td>
    <td style="padding:15px">Description
-------------

+ Registers a Cloud WAN branch device with a global network
+ This API i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAConfigurationTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a Cloud WAN configuration tag
+ This API is available in version 5</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConfigurationTagNames(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all CloudWAN configuration tag names.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAConfigurationTag(tagName, customCfg, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit a tag's configuration commands.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConfigurationTagsSummary(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all configuration tag and the branches that are attached to them</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationTagDetails(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the details of a Cloud WAN configuration tag
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachBranchesToAConfigurationTag(tagName, includeBranchList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach Cloud WAN branch devices to a global tag
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitAConfigurationTagToItsAttachedBranches(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Commit a Cloud WAN config tag configuration commands to all attached b</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAConfigurationTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete a Cloud WAN global tag
+ This API is available in version 5.3 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEncryptedPeerings(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the VPC peer pairs that have encrypted tunnels.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEncryptedPeering(vpcName1, vpcName2, haEnabled, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create encrypted peering.
+ This API is available in version 3.1 (for</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEncryptedPeering(vpcName1, vpcName2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete Encrypted Peering
+ This API is available in version 3.1 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peeringDiagnosticsTest(gw1, gw2, command, controllerGw, callback)</td>
    <td style="padding:15px">Description
-------------

+ Perform peering diagnostic test.
+ This API is available in version 4.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptedPeeringHASwitchOver(vpcName1, vpcName2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Force the fail over to the backup peering cloud.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitivePeering(source, nexthop, reachableCidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create transitive peering..
+ This API is available in version 3.0 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTransitivePeering(action, cID, callback)</td>
    <td style="padding:15px">Description
-------------

+ List transitive peering.
+ This API is available in version 3.0 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitivePeering(source, nexthop, reachableCidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete transitive peering.
+ This API is available in version 3.0 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAWSPeering(peer1AccountName, peer2AccountName, peer1Region, peer2Region, peer1VpcId, peer2VpcId, peer1RtbId, peer2RtbId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create AWS peering.
+ This API is available in version 3.1 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAWSPeerings(callback)</td>
    <td style="padding:15px">Description
-------------

+ List AWS peerings.
+ This API is available in version 3.1 or later rel</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAWSPeering(peer1VpcId, peer2VpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete AWS peering.
+ This API is available in version 3.1 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peerAzureVNets(reqAccountName, reqRegion, reqVpcId, accAccountName, accRegion, accVpcId, allowVirtualNetworkAccess1, allowVirtualNetworkAccess2, allowForwardedTraffic1, allowForwardedTraffic2, allowGatewayTransit1, allowGatewayTransit2, useRemoteGateways1, useRemoteGateways2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Peer Azure VNets.
+ This API is available in version 4.3 or later rele</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureNativePeerings(callback)</td>
    <td style="padding:15px">Description
-------------

+ List Azure Native Peerings.
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unpeerAzureVNetPeering(vnetName1, vnetName2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Unpeer Azure VNet peering.
+ This API is available in version 4.3 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSite2CloudConnection(vpcId, connectionName, connectionType, remoteGatewayType, tunnelType, sslServerPool, primaryCloudGatewayName, remoteGatewayIp, preSharedKey, localSubnetCidr, virtualLocalSubnetCidr, remoteSubnetCidr, virtualRemoteSubnetCidr, haEnabled, backupGatewayName, backupRemoteGatewayIp, backupPreSharedKey, phase1Auth, phase1DhGroup, phase1Encryption, phase2Auth, phase2DhGroup, phase2Encryption, privateRouteEncryption, routeTableList, remoteGatewayLatitude, remoteGatewayLongitude, backupRemoteGatewayLatitude, backupRemoteGatewayLongitude, enableIkev2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add Site2Cloud Connection.
+ This API is available in version 3.3 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPCAssociation(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all Site2Cloud tunnels and peering info.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSite2CloudConnections(transitOnly, callback)</td>
    <td style="padding:15px">Description
-------------

+ List Site2Cloud connections.
+ This API is available in version 3.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSite2CloudConnectionDetail(vpcId, connName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get Site2Cloud connection detail.
+ This API is available in version 3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSite2CloudConnection(vpcId, connName, networkType, cloudSubnetCidr, peerGeoInfo, phase1Identifier, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Site2Cloud connection info, like cloud subnet CIDR or geolocation</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBGPSite2CloudConnection(vpcOrVnetName, connectionName, remoteGwType, tunnelType, bgpEnabled, bgpRemoteAsNum, bgpAdvertisedNetworks, cgwInsideIp, vgwInsideIp, gwName, remoteGwIp, preSharedKey, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create a Site2Cloud connection with Primary-Cloud-Gateway BGP enabled.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSite2CloudConnectionAlgorithms(callback)</td>
    <td style="padding:15px">Description
-------------

+ List Site2Cloud algorithm options.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDownloadSite2CloudConfig(site2cloudConnectionName, vpcId, gatewayVendor, gatewayPlatform, gatewaySoftware, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get/download Site2Cloud config.
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSite2CloudConnection(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete Site2Cloud connection.
+ This API is available in version 3.3 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSite2CloudConnectionKey(vpcId, connName, transitOnly, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get Site2Cloud connection key.
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVGWConnectionDetail(connName, vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get VGW connection detail.
+ This API is available in version 4.3 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDPDConfig(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable DPD for an existing Site2Cloud connection
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDPDConfig(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable DPD for an existing Site2Cloud connection.
+ This API is avail</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableActiveActiveHA(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable active active HA on a Site2Cloud connection.
+ This API is avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableActiveActiveHA(vpcId, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable active active HA on a Site2Cloud connection.
+ This API is ava</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runSite2CloudDiagnostic(vpcId, gatewayName, actionName, connectionName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Run Site2Cloud diagnostic.
+ This API is available in version 3.3 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadTheVPNConfiguration(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetVPNConfiguration(vpcId, username, lbName, samlEndpoint, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the VPN configuration file name.
+ To download the configuration,</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadTheVPNUsersCSVFile(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetVPNUsersFile(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the VPN Users file name.
+ To download the configuration, copy th</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLBNames(vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API either lists ELB names attached to a VPC or lists VPN gateway</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNPKIMode(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets VPN PKI Mode.
+ This API is available in version 5.0 or later rel</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNUsers(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all VPN users in the controller.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLiveVPNUsers(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the live VPN users.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLiveVPNUsersDetails(username, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the live VPN users details.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importVPNUsers(csvFile, vpcId, lbName, userEmail, profileName, dns, samlEndpoint, callback)</td>
    <td style="padding:15px">Description
-------------

+ General description for this Aviatrix API.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVPNUserAndAttachItToProfile(vpcId, username, userEmail, profileName, lbName, dns, samlEndpoint, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a VPN user and bind it to a VPC. Attaching the user to a profile i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNUserByName(username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get VPN user by name.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVPNUser(vpcId, username, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete a VPN user.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGEOVPNUser(vpcId, username, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete a VPN user.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachVPNUser(dns, vpcIdOrDnsName, lbName, username, userEmail, samlEndpoint, useProfile, profileName, externalUser, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach VPN User.
+ This API is available in version 3.1 or later relea</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachVPNUser(dns, vpcIdOrDnsName, username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach VPN User.
+ This API is available in version 3.1 or later relea</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSAMLEndpoint(endpointName, idpMetadataType, idpMetadata, entityId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create a SAML endpoint.
+ This API is available in version 3.3 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSAMLEndpoints(callback)</td>
    <td style="padding:15px">Description
-------------

+ List SAML endpoints.
+ This API is available in version 3.3 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSAMLEndpointNames(callback)</td>
    <td style="padding:15px">Description
-------------

+ List SAML endpoint names only.
+ This API is available in version 4.1</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSAMLEndpoint(endpointName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete SAML endpoint.
+ This API is available in version 3.3 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSAMLEndpointInformation(endpointName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets SAML Endpoint Information
+ This API is available in version 5.0</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGEOVPN(accountName, domainName, cname, elbDnsName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable GEO VPN
+ This API is available in version 3.2 or later release</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGEOVPN(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable GEO VPN.
+ This API is available in version 3.2 or later relea</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addELBToGEOVPN(elbDnsName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Adds a load balancer to Geo VPN.
+ This API is available in version 5.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteELBFromGEOVPN(elbDnsName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Deletes a load balancer from Geo VPN.
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllELBDNSNames(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets all ELB DNS names that can be added to GeoVPN.
+ This API is avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGEOVPNInfo(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the GEO VPN configuration
+ This API is available in version 5.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUDPLoadbalancer(accountName, hostedZoneName, vpnServiceName, gws0, gws1, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create UDP loadbalancer.
+ This API is available in version 3.3 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUDPLoadBalancers(callback)</td>
    <td style="padding:15px">Description
-------------

+ List UDP LoadBalancers.
+ This API is available in version 3.3 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUDPLoadBalancer(accountName, hostedZoneName, vpnServiceName, gws0, gws1, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete UDP LoadBalancer.
+ This API is available in version 3.3 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUserDefinedEmailNotification(fileName, emailContent, enabled, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set user defined email notification.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUserVPNLicenseThresholdNotification(licensePurchased, licenseThreshold, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set user VPN license threshold notification.
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNUserAccelerator(callback)</td>
    <td style="padding:15px">Description
-------------

+ List VPN user accelerator.
+ This API is available in version 4.3 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVPNUserAccelerator(endpoints, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update VPN User Accelerator.
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProfilesMembers(callback)</td>
    <td style="padding:15px">Description
-------------

+ List profiles and their members
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNUsersNotAttachedToAProfile(vpcId, profileName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List VPN users that are not attached to a profile
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNUsersAttachedToAProfile(vpcId, profileName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List VPN users that are attached to a profile
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addProfileMember(profileName, username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a user to a profile as a profile member.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProfileMember(profileName, username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach a user from a profile.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProfilePoliciesAndBasePolicy(profileName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the policies set for a profile and base policy.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserProfile(profileName, basePolicy, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a new user profile.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserProfile(profileName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete an existing user profile
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProfilePolicy(profileName, policy, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update security policy of an existing profile.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileBasePolicy(profileName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get profile base policy.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProfilePolicies(profileName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the policies set for a profile.	
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllVPCSubnets(accountName, region, vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all VPC subnets
+ This API is available in version 5.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNGatewayConfiguration(vpcId, lbOrGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get VPN gateway configuration.
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveTheVPNDuplicateConnectionConfigurationForTheGatewayLBDNS(vpcId, lbName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Retrieve the VPN Duplicate Connection Configuration for the Gateway/LB</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVPNConfiguration(command, vpcId, lbName, key, value, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit VPN configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVPNGatewayPBR(vpcId, lbOrGatewayName, pbrSubnetCidr, pbrDefaultGatewayIp, natTranslationLoggingEnabled, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable VPN gateway PBR.
+ This API is available in version 3.3 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableVPNGatewayPBR(vpcId, lbOrGatewayName, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable VPN gateway PBR.
+ This API is available in version 3.3 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setVPNMaxConnection(vpcId, lbOrGatewayName, maxConnections, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set VPN max connection.
+ This API is available in version 3.3 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVPNClientCertificateSharing(vpcId, lbOrGatewayName, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable VPN client certificate sharing.
+ This API is available in vers</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNClientCertificateSharingStatus(vpcId, lbOrGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get VPN client certificate sharing status.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableVPNClientCertificateSharing(vpcId, lbOrGatewayName, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable VPN client certificate sharing.
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNGatewayAuthenticationConfiguration(vpcId, lbOrGatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get VPN gateway authentification configuration.
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setVPNGatewayAuthentication(vpcId, lbOrGatewayName, dns, authType, oktaUrl, oktaToken, oktaUsernameSuffix, duoIntegrationKey, duoSecretKey, duoApiHostname, duoPushMode, ldapServer, ldapBindDn, ldapPassword, ldapBaseDn, ldapUsernameAttribute, ldapUseSsl, ldapClientCert, ldapCaCert, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set VPN gateway authentification.
+ This API is available in version 3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySplitTunnel(vpcId, command, lbName, splitTunnel, additionalCidrs, nameservers, searchDomains, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Modify split tunnel configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reloadDHCPConfiguration(vpcId, lbName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Reload DHCP configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setVPNClientCIDR(vpcId, lbOrGatewayName, cidr, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set VPN client CIDR.
+ This API is available in version 3.3 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDuoConfig(vpcId, lbOrGatewayName, dns, lbName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get DUO configuration.
+ This API is available in version 3.4 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDuoConfig(vpcId, lbOrGatewayName, integrationKey, secretKey, apiHostname, pushMode, saveTemplate, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Duo configuration.
+ This API is available in version 3.4 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOktaConfig(vpcId, lbOrGatewayName, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get Okta configuration.
+ This API is available in version 3.4 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editOktaConfig(vpcId, lbOrGatewayName, oktaUrl, oktaToken, oktaUsernameSuffix, saveTemplate, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit Okta configuration.
+ This API is available in version 3.4 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTheVPNDuplicateConnectionConfigurationForTheGatewayLBDNS(vpcId, lbName, status, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Retrieve the VPN duplicate connection configuration for the gateway/LB</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableNATOnAVPNGateway(vpcId, lbOrGatewayName, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable VPN NAT on a VPN Gateway
+ This API is available in version 5.0</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableNATOnAVPNGateway(vpcId, lbOrGatewayName, dns, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable VPN NAT on a VPN Gateway
+ This API is available in version 5.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNClientVersions(vpcId, lbName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all Aviatrix VPN client versions that the set_minimum_vpn_client_</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setMinimumVPNClientVersion(vpcId, lbName, version, callback)</td>
    <td style="padding:15px">Description
-------------

+ Sets a minimum Aviatrix VPN client version that the VPN gateway can su</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNEnabledVPCsWithDNS(withGw, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists the VPC/Vnet ID's that are used on the controller and the curren</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchVPNUserHistory(usernames, destinationIps, startTime, endTime, gatewayNames, callback)</td>
    <td style="padding:15px">Description
-------------

+ Search VPN user connection history base on time, destination IP, etc 
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVPNUserDiagnostics(username, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show the filtered user VPN access diagnostics.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPolicyTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add policy tag.
+ This API is available in version 3.1 or later releas</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyMembers(tagName, newPoliciesIName, newPoliciesICidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update policy members.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyMembers(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List policy members.
+ This API is available in version 3.1 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete policy tag.
+ This API is available in version 3.1 or later rel</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vPCAccessPolicy(vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the access policy items for a gateway.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setVPCAccessBasePolicy(vpcName, basePolicy, basePolicyLogEnable, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set/update base access policy for a gateway.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStatefulFirewallRules(gatewayName, rules, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update access policy of a gateway.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appendStatefulFirewallRules(gatewayName, rules, callback)</td>
    <td style="padding:15px">Description
-------------

+ Append a list of existing policies 
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStatefulFirewallRules(gatewayName, rules, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete a list of existing policies
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertStatefulFirewallRules(gatewayName, rules, indexRule, callback)</td>
    <td style="padding:15px">Description
-------------

+ Insert a list of new policies at the reference point
+ This API is ava</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewaySecurityPolicyRules(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists a gateway's security policy rules
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadFQDNLog(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetFQDNLogFileName(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the FQDN log file name of a specified gateway.
+ To download the</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadFQDNDiscoveryFile(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetFQDNDiscoveryFileName(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Downloads FQDN discovery file of a specified gateway
+ To download the</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetFQDNFilterTagDomainNameFile(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets FQDN FQDN Filter Tag Domain Name file of a specified tag
+ To dow</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadFQDNFilterTagDomainNameFile(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFQDNFilterTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add FQDN filter tag.
+ This API is available in version 3.1 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setFQDNFilterTagDomainNames(tagName, domainNames0Fqdn, domainNames0Proto, domainNames0Port, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set FQDN filter tag domain names.
+ This API is available in version 3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFQDNFilterTagDomainNames(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List FQDN filter tag domain names.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setFQDNFilterTagColor(tagName, color, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set FQDN filter tag color.
+ This API is available in version 3.1 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDisableFQDNTag(tagName, status, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable/disable an FQDN tag.
+ This API is available in version 3.1 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachFQDNFilterTagToGateway(tagName, gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Attach a FQDN filter tag to a gateway.
+ This API is available in vers</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachedGatewaysForFQDNFilterTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List FQDN filter tag attached gateways.
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewaysNotAttachedToFQDNFilterTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists gateways not attached to the specified FQDN filter tag.
+ This A</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFQDNFilterTags(callback)</td>
    <td style="padding:15px">Description
-------------

+ List FQDN filter tags.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachFQDNFilterTagFromGateway(gwName, tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Detach FQDN filter tag from gateway.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFQDNFilterTag(tagName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete FQDN filter tag.
+ This API is available in version 3.1 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFQDNExceptionRuleStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the status indicating whether FQDN exception rule is enabled or no</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableFQDNExceptionRule(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API enables FQDN exception rule
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableFQDNExceptionRule(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API disables the FQDN exception rule.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllGatewaysForFQDNFilterTag(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API lists all the gateways for FQDN filter.
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFQDNFilterTagSourceIPFilters(tagName, gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API lists all the source IP filters for a FQDN filter tag.
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFQDNFilterTagSourceIPFilters(gatewayName, tagName, sourceIps, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API updates source IP filters for a FQDN filter tags
+ This API i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startFQDNDiscovery(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Start FQDN discovery.
+ This API is available in version 3.3 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFQDNDiscoveryResult(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show FQDN discovery result.
+ This API is available in version 3.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopFQDNDiscovery(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Stop FQDN discovery.
+ This API is available in version 3.3 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSupportedProtocolsForFQDNFilterTag(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API lists all the supported protocols for FQDN filter.
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFQDNPrivateNetworkFilteringStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the status of RFC1918 CIDRs for FQDN filter tag.
+ This API is ava</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableFQDNFilterOnPrivateNetworks(callback)</td>
    <td style="padding:15px">Description
-------------

+ Include RFC1918 cidrs for egress filtering
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableFQDNFilterOnPrivateNetworks(callback)</td>
    <td style="padding:15px">Description
-------------

+ Exclude RFC1918 cidrs for egress filtering
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableFQDNOnCustomNetworks(sourceIps, callback)</td>
    <td style="padding:15px">Description
-------------

+ 
+ This API is available in version 5.3 or later releases.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAWSGuardDuty(callback)</td>
    <td style="padding:15px">Description
-------------

+ List AWS GuardDuty.
+ This API is available in version 3.5 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAWSGuardDuty(accountName, region, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable AWS GuardDuty.
+ This API is available in version 3.5 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAWSGuardDuty(accountName, region, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable AWS GuardDuty.
+ This API is available in version 3.5 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAWSGuardDutyPollInterval(interval, callback)</td>
    <td style="padding:15px">Description
-------------

+ Updates the AWS guard duty polling interval time.
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAWSGuardDutyExcludedIPs(accountName, region, excludedIps, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update the excluded IP address list for guard duty. The excluded IP ad</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPrivateS3Gateways(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all AWS primary private S3 gateways
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrivateS3GatewayDetails(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get configured buckets and onprem access CIDRS (sources)  for a gatewa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivateS3(gatewayName, onpremCidrs, s3BucketUrls, callback)</td>
    <td style="padding:15px">Description
-------------

+ Updates (a source and/or bucket urls for) a gateway for PrivateS3 file</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enablePrivateS3(gatewayName, onpremCidrs, s3BucketUrls, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enables (a source and/or bucket urls for) a gateway for PrivateS3 file</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePrivateS3(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disables/removes a gateway's private S3 file transfer participation.
+</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPublicSubnetFilteringGateway(accountName, region, vpcId, gatewaySubnet, gatewayName, gatewaySize, routeTable, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a public-subnet-filtering gateway
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicSubnetFilteringGatewayDetails(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the details of an existing public-subnet-filtering gateway
+ This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPublicSubnetFilteringGateways(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all existing public-subnet-filtering gateways
+ This API is avail</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePublicSubnetFilteringGateway(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete an existing public-subnet-filtering gateway
+ This API is avail</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enablePublicSubnetFilteringGuardDutyEnforcredMode(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable blocking AWS Guard Duty found malicious IP adress on a public-s</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePublicSubnetFilteringGuardDutyEnforcredMode(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable blocking AWS Guard Duty found malicious IP adress on a public-</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editPublicSubnetFilteringEnforcedRouteTableList(gatewayName, routeTable, callback)</td>
    <td style="padding:15px">Description
-------------

+ Edit the existing public-subnet-filtering gateway's enforced route tab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableHAForAPublicSubnetFilteringGateway(gatewayName, gatewaySubnet, routeTables, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable HA for public subnet filtering gateway
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFQDNStatistics(gatewayName, duration, details, callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the FQDN accetped and blocked hostname stats
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteACustomVPC(poolName, accountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete a custom VPC. Only valid for Userconnect.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOCIGatewayImageFromRegion(accountName, vpcRegion, callback)</td>
    <td style="padding:15px">Description
-------------

+ Removes the OCI gateway images from a region
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadVPCTrackerList(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file "aviatrix_vp</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1ExportVPCTrackerListToCSV(callback)</td>
    <td style="padding:15px">Description
-------------

+ Exports the VPC tracker list to a CSV file
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testCIDROverlap(cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Test CIDR overlapping.
+ This API is available in version 3.4 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1ListOrGetTheTerraformConfiguration(operation, resource, callback)</td>
    <td style="padding:15px">Description
-------------

+ Export the terraform configuration.
+ To download the configuration, c</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadTheTerraformConfiguration(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVersionInfo(callback)</td>
    <td style="padding:15px">Description
-------------

+ List the latest available version and current version of the controlle</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeController(version, callback)</td>
    <td style="padding:15px">Description
-------------

+ Upgrade the controller to a specific release or to the latest.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeDryRun(version, callback)</td>
    <td style="padding:15px">Description
-------------

+ Upgrade dry run to a specific release or to the latest.
+ This API is</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerAutoUpgradeOnRebootConfig(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get controller auto upgrade on reboot config.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewayUpgradeStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ List the upgrade status of all the gateways.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableUpgradeOnReboot(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable upgrade on reboot.
+ This API is available in version 3.2 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableUpgradeOnReboot(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable upgrade on reboot.
+ This API is available in version 3.2 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestReleaseVersion(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the latest released version.
+ This API is available in version 5</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableBackupConfiguration(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable backup configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableBackupConfiguration(accountName, storageName, containerName, bucketName, now, multiple, region, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable backup configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreConfiguration(fileName, accessKey, secretKey, accountName, bucketName, storageName, containerName, subscriptionId, certificatePath, armSubscriptionId, armApplicationEndpoint, armApplicationClientId, armApplicationClientSecret, ociUserId, ociTenancyId, ociCompartmentId, region, callback)</td>
    <td style="padding:15px">Description
-------------

+ Restore configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupConfiguration(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get backup configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateToANewController(callback)</td>
    <td style="padding:15px">Description
-------------

+ Migrate controller OS to a new 18.04 instance and shutdown the current</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertControllerEIP(callback)</td>
    <td style="padding:15px">Description
-------------

+ Revert controller EIP back to the old controller and shutdown current</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllSecurityPatches(update, gatewayName, controller, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the security patch status of a gateway or controller or updates t</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applySecurityPatch(patch, callback)</td>
    <td style="padding:15px">Description
-------------

+ Apply given patch to controllers and/or gateways.
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllSoftwarePatches(update, gatewayName, controller, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the security patch status of a gateway or controller or updates t</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applySoftwarePatch(patch, callback)</td>
    <td style="padding:15px">Description
-------------

+ Apply given patch to controllers and/or gateways.
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerLicenseType(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get controller type (like BYOL meter, etc...).
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerID(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get an Aviatrix controller's UUID
+ This API is available in version 4</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAWSMeteringInfo(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get AWS metering info
+ This API is available in version 5.0 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshCustomerID(callback)</td>
    <td style="padding:15px">Description
-------------

+ Refreshes the customer ID.
+ This API is available in version 5.1 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusChangeEventEmail(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the status change event email.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailNotificationConfiguration(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the email notification configuration.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setStatusChangeEventEmail(callback)</td>
    <td style="padding:15px">Description
-------------

+ Set status change event email.
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showLDAPConfiguration(vpcId, lbName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show LDAP configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyLDAPConfiguration(vpcId, lbName, enableLdap, ldapServer, ldapUseSsl, ldapClientCert, ldapCaCert, ldapBindDn, ldapPassword, ldapBaseDn, ldapUsernameAttribute, ldapAdditionalLdapReq, saveTemplate, callback)</td>
    <td style="padding:15px">Description
-------------

+ Modify LDAP Configuration.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testLDAPBind(vpcId, lbName, ldapServer, ldapBindDn, ldapPassword, ldapBaseDn, ldapUsernameAttribute, ldapUser, ldapAdditionalReq, ldapUseSsl, ldapClientCert, ldapCaCert, ldapClientCertPath, callback)</td>
    <td style="padding:15px">Description
-------------

+ Test LDAP connection.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerSecurityGroupManagementStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get controller security-group management status.
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableControllerSecurityGroupManagement(accessAccountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable controller security-group management.
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableControllerSecurityGroupManagement(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable controller security-group management
+ This API is available i</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBellAlertConfig(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get bell alert configurations.
+ This API is available in version 5.0</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBellAlertConfig(statusCidrCheck, statusGuardDuty, statusSyslogCheck, statusRouteLimitCheck, statusRouteBlackholeCheck, callback)</td>
    <td style="padding:15px">Description
-------------

+ Saves the current alert bell display.
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerVPCDNSServerStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the controller's VPC DNS Server status
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableControllerVPCDNSServerStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enables the controller's VPC DNS Server status
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableControllerVPCDNSServerStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disables the controller's VPC DNS Server status
+ This API is availabl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setControllerName(controllerName, callback)</td>
    <td style="padding:15px">Set Controller Name</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerAWSAccountNumber(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the number of controller AWS Accounts.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showIfControllerIsSaaS(callback)</td>
    <td style="padding:15px">Description
-------------

+ Show if the controller is created on SaaS platform or not.
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerName(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the name of the current controller.
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSplunkLogging(serverIp, port, cuOutputCfg, customInputCfg, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable splunk logging.
+ This API is available in version 3.2 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSplunkLoggingStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get splunk logging status.
+ This API is available in version 3.2 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSplunkLogging(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable splunk logging.
+ This API is available in version 3.2 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableLogStashForwarder(serverType, forwarderType, serverIp, port, trustedCa, configFile, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable LogStash Forwarder
+ This API is available in version 3.3 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogStashForwarderStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get LogStash Forwarder Status.
+ This API is available in version 3.3</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableLogStashForwarder(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable LogStash Forwarder.
+ This API is available in version 3.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSumologicLogging(accessId, accessKey, sourceCategory, customCfg, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable sumologic logging
+ This API is available in version 3.2 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumologicLoggingStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get sumologic logging status.
+ This API is available in version 3.2 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSumologicLogging(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable Sumologic Logging
+ This API is available in version 3.2 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableRemoteSyslog(server, port, protocol, caCertificate, publicCertificate, privateKey, template, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable remote syslog.
+ This API is available in version 3.2 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteSyslogStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get remote syslog status.
+ This API is available in version 3.2 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableRemoteSyslog(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable remote syslog.
+ This API is available in version 3.1 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDatadogAgent(apiKey, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable datadog agent.
+ This API is available in version 3.2 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatadogAgentStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get Datadog agent status.
+ This API is available in version 3.2 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDatadogAgent(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disables datadog agent
+ This API is available in version 3.2 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableNetflowAgent(serverIp, port, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable Netflow agent.
+ This API is available in version 3.5 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetflowAgentStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get netflow agent status.
+ This API is available in version 3.5 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableNetflowAgent(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable Netflow agent.
+ This API is available in version 3.5 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableCloudWatchAgent(cloudwatchRoleArn, region, excludeGatewayList, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable CloudWatch agent.
+ This API is available in version 4.0 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudWatchAgentStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get CloudWatch agent status.
+ This API is available in version 4.0 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableCloudWatchAgent(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable CloudWatch agent.
+ This API is available in version 4.0 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSpanPort(gatewayName, bucketName, retentionPeriod, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add a new span port.
+ This API is available in version 4.1 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSpanPorts(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the span ports.
+ This API is available in version 4.1 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSpanPort(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete an existing span port.
+ This API is available in version 4.1 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showTunnelStatusChangeDetectionTime(entity, callback)</td>
    <td style="padding:15px">Description
-------------

+ Show tunnel status change time.
+ This API is available in version 5.1</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusOfPeerDeletionWithSpokeAttached(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get status of peer deletion with spoke attached.
+ This API is availab</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enablePeerDeletionWithSpokeAttached(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable peer deletion with spoke attached.
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePeerDeletionWithSpokeAttached(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable peer deletion with spoke attached.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setKeepaliveSpeed(speed, callback)</td>
    <td style="padding:15px">Description
-------------

+ Set Keepalive Speed.
+ This API is available in version 3.2 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusOfKeepaliveSpeed(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the status of keep-alive speed.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createImportCertificateSigningRequest(fqdnName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create/Import Certificate Signing Request
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCertificates(caCert, serverCert, privateKey, callback)</td>
    <td style="padding:15px">Description
-------------

+ Import certificates.
+ This API is available in version 4.7 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableImportedCertificate(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable the imported certificate on a controller or gateway.
+ This AP</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableCertificateChecking(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable certificate checking.
+ This API is available in version 3.2 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableCertificateChecking(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable certificate checking.
+ This API is available in version 3.2 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateCheckingStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get certificate checking status.
+ This API is available in version 3.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerApacheVersion(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get controller Apache version.
+ This API is available in version 4.1</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTLSVersions(callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets TLS versions
+ This API is available in version 5.0 or later rele</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTLSVerison(version, callback)</td>
    <td style="padding:15px">Description
-------------

+ Sets which TLS version is supported.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewaysWithImportedCERT(callback)</td>
    <td style="padding:15px">Description
-------------

+ List gateways that have imported CERT
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayAuditConfigiuration(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API returns the background gateway audit control configuration, s</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGatewayBackroundAudit(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable Gateway background audit
+ This API is available in version 5.0</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGatewayBackgroundAudit(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable the Gateway background audit
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGatewayBackgroundAuditEmailNotification(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable the gateway background audit Email Notification
+ This API is a</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGatewayBackgroundAuditEmailNotification(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable gateway background audit Email Notification
+ This API is avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableFIPSMode(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable the FIPS mode.
+ This API is available in version 4.6 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableFIPSMode(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable the FIPS mode.
+ This API is available in version 4.6 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentFIPSMode(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the Current FIPS Mode.
+ This API is available in version 4.6 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshCredentials(callback)</td>
    <td style="padding:15px">Description
-------------

+ Refresh credentials.
+ This API is available in version 4.6 or later r</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetControllerAuditLogFile(search, callback)</td>
    <td style="padding:15px">Description
-------------

+ Gets the controller audit log file name
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadControllerAuditLog(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadTraceLog(vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Upload Trace logs. Valid for UCC and CloudN. If vpc_name is specified</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAviatrixControllerCommandlog(search, logType, callback)</td>
    <td style="padding:15px">Description
-------------

+ Display Aviatrix controller commandlog
+ This API is available in vers</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerAuditLog(search, callback)</td>
    <td style="padding:15px">Description
-------------

+ Display command log audit
+ This API is available in version 4.1 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step2DownloadThePackageCapture(filename, callback)</td>
    <td style="padding:15px">Description
-------------

+ Select "Send and Download" to download the specified file from Step 1
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1GetPackageCaptureFile(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Download gateway package capture results.
+ This API is available in v</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingDiagnostics(gatewayName, hostName, interface_, callback)</td>
    <td style="padding:15px">Description
-------------

+ Perform network diagnostics using the ping command.
+ This API is avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerPublicIP(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get controller public IP.
+ This API is available in version 5.0 or la</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateControllerIP(previousIp, forceUseGivenIp, callback)</td>
    <td style="padding:15px">Description
-------------

+ Migrate controller IP.
+ This API is available in version 4.0 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testNetworkConnectivity(gatewayName, hostName, protocol, port, callback)</td>
    <td style="padding:15px">Description
-------------

+ Test reachablity of destination IP addresses or hostnames from gateway</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPackageCapture(gatewayName, interface_, duration, packetLength, port, hostName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Start gateway package capture
+ This API is available in version 5.0 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopPackageCapture(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Stop gateway package capture
+ This API is available in version 5.0 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tracepathDiagnostics(gatewayName, hostName, interface_, callback)</td>
    <td style="padding:15px">Description
-------------

+ Perform path MTU discovery using tracepath command.
+ This API is avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tracerouteDiagnostics(gatewayName, hostName, interface_, callback)</td>
    <td style="padding:15px">Description
-------------

+ Perform network diagnostics using traceroute command.
+ This API is av</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewayInterfaces(gatewayName, forPing, callback)</td>
    <td style="padding:15px">Description
-------------

+ List interfaces on a gateway.
+ This API is available in version 4.1 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runDiagnostics(controller, gateway, dmzVpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Perform network diagnostics.
+ This API is available in version 4.3 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDiagnostics(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the last diagnostics result.
+ This API is available in version 4.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitDiagnostics(callback)</td>
    <td style="padding:15px">Description
-------------

+ Submit the last diagnostics result.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceGateway(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Replace gateway.
+ This API is available in version 3.1 or later relea</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateGatewayPublicIPAddress(gatewayName, publicIp, callback)</td>
    <td style="padding:15px">Description
-------------

+ Migrate/change a gateway's public IP address
+ When a gateway's public</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeAGateway(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Upgrade a gateway.
+ This API is available in version 5.0 or later rel</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGatewayServices(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists gateway services.
+ This API is available in version 5.0 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartGatewayService(gatewayName, serviceName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Restarts a specific service of a given gateway
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayServiceStatus(gatewayName, serviceName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Retrieve the status of a service.
+ This API is available in version 5</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGatewayRollbackOnErrorStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get the current keep-gateway-on-error configuration status.
+ This API</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableGatewayRollbackOnError(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API will not roll back resources after a gateway creation error.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableGatewayRollbackOnError(callback)</td>
    <td style="padding:15px">Description
-------------

+ This API will roll back resources after a gateway creation error.
+ Th</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAWSAccountDiagnostics(callback)</td>
    <td style="padding:15px">Description
-------------

+ Shows issues with the account and gateways.
+ This API is available in</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showOrSendVPCDiagnostics(accountName, region, vpcId, sendResults, callback)</td>
    <td style="padding:15px">Description
-------------

+ Shows VPC Diagnostics with resource information and can send those dia</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartRsyslogService(callback)</td>
    <td style="padding:15px">Description
-------------

+ Restart rsyslog service.
+ This API is available in version 3.5 or lat</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableBGPConfiguration(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable BGP configuration for a particular gateway.
+ This API is avail</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableBGPConfiguration(gwName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable BGP configuration for a particular Aviatrix gateway.
+ This AP</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableBGPSwitchOver(gatewayName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable BGP switchover.
+ This API is available in version 3.3 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateInstances(cloudType1, cloudType2, accountName1, accountName2, region1, region2, vpcId1, vpcId2, publicSubnet1, publicSubnet2, callback)</td>
    <td style="padding:15px">Description
-------------

+ Validate instances based on cloud type, account name, region, VPC ID.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectivityTestStep(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get connectivity test step.
+ This API is available in version 4.2 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateInstances(callback)</td>
    <td style="padding:15px">Description
-------------

+ Terminate instances on a controller.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFlightpathSourceInstances(accountName, region, vpcIdName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the source instances for the flightpath connection test.
+ Th</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFlightpathDestinationInstances(accountName, region, vpcIdName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the destination instances for the flightpath connection test.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flightpathConnectionTest(srcAccount, srcCloudType, srcRegion, srcZone, srcVpcId, srcPrivateIp, srcInstanceId, destAccount, destCloudType, destRegion, destZone, destVpcId, destPrivateIp, destInstanceId, protocol, port, interface_, callback)</td>
    <td style="padding:15px">Description
-------------

+ Test flightpath connection and return results.
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flightpathOnpremConnectionTest(srcIp, srcAccount, srcCloudType, srcRegion, srcZone, srcVpcId, srcPrivateIp, srcInstanceId, destIp, destAccount, destCloudType, destRegion, destZone, destVpcId, destPrivateIp, destInstanceId, protocol, port, interface_, contentType, callback)</td>
    <td style="padding:15px">Description
-------------

+ Tests a flightpath onprem connection and displays the instance to/from</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitFlightpathResult(callback)</td>
    <td style="padding:15px">Description
-------------

+ Submit the flightpath result to Aviatrix support.
+ This API is availa</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBellAlerts(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the bell alerts.	
+ This API is available in version 3.5 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBellAlert(messageId, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete one/all bell alert(s)
+ This API is available in version 3.5 or</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllowedSubnetsForVPC(vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the allowed subnets for a datacenter extension VPC. Only avai</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allowSubnetsToVPC(gwName, cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Allow subnets to VPC. Only available in CloudN.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnetsFromVPC(gwName, cidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Delete subnets to VPC. Only available in CloudN.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setupMaximumNumberOfVPCVNets(vpcNum, callback)</td>
    <td style="padding:15px">Description
-------------

+ Setup number of VPCs that customer is planning to create in AWS. This</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMaximumNumberOfVPCVNets(callback)</td>
    <td style="padding:15px">Description
-------------

+ List the configured number of VPCs that customer is planning to create</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAvailableCIDRs(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all CIDRs on the CloudN system. One of these CIDRs has to be used</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDatacenterExtension(accountName, vpcName, vpcReg, vpcNet, vpcSize, internetAccess, publicSubnet, tunnelType, vpnAccess, vpnCidr, intraVmRoute, otpMode, vpnUserEmailDomain, duoIntegrationKey, duoSecretKey, duoApiHostname, duoPushMode, oktaUrl, oktaToken, oktaUsernameSuffix, enableElb, enableClientCertSharing, maxConn, splitTunnel, additionalCidrs, nameservers, searchDomains, enableLdap, ldapServer, ldapBindDn, ldapPassword, ldapBaseDn, ldapUserAttr, ldapAdditionalReq, ldapUseSsl, ldapClientCert, ldapCaCert, callback)</td>
    <td style="padding:15px">Description
-------------

+ Create a datacenter extension. Only available in CloudN.
+ Note: Use d</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudNNetworkInterfaces(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all network interfaces in CloudN
+ This API is available in vers</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setCloudNInterfaceAddress(interface_, operation, callback)</td>
    <td style="padding:15px">Description
-------------

+ Sets CloudN Interface Address
+ This API is available in version 5.0 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudNNetworkInterfaceDetails(callback)</td>
    <td style="padding:15px">Description
-------------

+ Lists all network interface details in CloudN
+ This API is available</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setCloudNInterfaceAddressDetails(interface_, ip, netmask, gateway, dns1, dns2, mtu, callback)</td>
    <td style="padding:15px">Description
-------------

+ Sets CloudN interface address details
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanAWSIAMPolicy(accessAccountName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Scan AWS IAM Policy
+ This API is available in version 4.2 or later re</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAWSIAMPolicyAutoUpdateStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get AWS IAM Policy Auto Update Status
+ This API is available in versi</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAccessAccountIAMPolicyUpdate(callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable access account IAM policy update
+ This API is available in ver</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAutoAWSIAMPolicyUpdate(callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable auto AWS IAM policy update.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIAMPolicyUpdateRecord(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get IAM policy update record.
+ This API is available in version 4.2 o</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessAddressPoolCIDR(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the available access address pool CIDRs. Only valid for Userc</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dNSCnameAction(command, ip, cname, callback)</td>
    <td style="padding:15px">Description
-------------

+ Add, modify, or delete DNS Cnames. Only valid for Userconnect.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessAddressPool(command, vpcName, globalIp, historyLimit, callback)</td>
    <td style="padding:15px">Description
-------------

+ List free, mapped or all access address pool. Only valid for Userconne</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aWSDomainNameSystem(command, domainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable or disable AWS DNS. Only valid for Userconnect.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNonManagementVPCs(callback)</td>
    <td style="padding:15px">Description
-------------

+ List all the non-management VPCs. Only valid for Userconnect.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setupAccessAddressPool(accessAddressPool, managementVpc, callback)</td>
    <td style="padding:15px">Description
-------------

+ Setup access address pool. Only valid for Userconnect.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudOpsUnmappedInstances(vpcName, callback)</td>
    <td style="padding:15px">Description
-------------

+ List all CloudOps unmapped instances. Only valid for Userconnect.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudOpsScanMapTarget(command, srcVpcName, destVpcName, srcVpcCidr, dstVpcCidr, callback)</td>
    <td style="padding:15px">Description
-------------

+ Automatically map free access addresses or unmap mapped addresses. Onl</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudOpsMapTargetSingleInstance(srcVpcName, destVpcName, freeAccessAddress, ip, nameTag, callback)</td>
    <td style="padding:15px">Description
-------------

+ Manually map a free access address to single instance. Only valid for</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudOpsUnmapTargetSingleInstance(srcVpcName, mappedAccessAddress, callback)</td>
    <td style="padding:15px">Description
-------------

+ Manually unmap single instance and free mapped access address. Only va</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peeringClusterPairAction(subaction, clusterName1, clusterName2, callback)</td>
    <td style="padding:15px">Description
-------------

+ cluster peering pair actions such as list or peer and unpeer for clust</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminatePeeringCluster(clusterName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Terminate peering cluster.
+ This API is available in version 4.6 or l</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchPeeringCluster(accountName, vpcReg, vpcId, publicSubnet, gwList, clusterName, maxPeerInst, callback)</td>
    <td style="padding:15px">Description
-------------

+ Launch peering cluster
+ This API is available in version 4.6 or later</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peeringClusterAction(subaction, clusterName, gwList, numGwDel, callback)</td>
    <td style="padding:15px">Description
-------------

+ Peering cluster actions such as list, add and delete gateways for exis</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">controllerSAMLLogin(username, password, callback)</td>
    <td style="padding:15px">Description
-------------

+ This API authenticates SAML login credentials for the controller.
+ Th</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSAMLLoginStatus(callback)</td>
    <td style="padding:15px">Description
-------------

+ This parameter indicates which Aviatrix API is being invoked.
+ This A</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listControllerSAMLLogin(callback)</td>
    <td style="padding:15px">Description
-------------

+ Get if SAML login is enabled or not.
+ This API is available in versio</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGatewayExtendedPublicCIDRs(gatewayName, extendedCidrs, callback)</td>
    <td style="padding:15px">Description
-------------

+ Updates the extended public CIDRs of a specifed gateway.
+ This API is</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyBGPGatewaySASN(gwName, num, callback)</td>
    <td style="padding:15px">Description
-------------

+ Modify BGP Gateway's ASN (autonomous system number).
+ This API is ava</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableEgressOnTGWRouteDomain(tgwName, routeDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Disable egress on TGW route domain.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableEgressOnTGWRouteDomain(tgwName, routeDomainName, callback)</td>
    <td style="padding:15px">Description
-------------

+ Enable egress on TGW route domain.
+ This API is available in version</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVPCAccessPolicy(vpcName, newPolicy, callback)</td>
    <td style="padding:15px">Description
-------------

+ Update access policy of a gateway.
</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPCRouteTables(accountName, vpcRegion, vpcId, callback)</td>
    <td style="padding:15px">Description
-------------

+ List VPC's route tables
+ This API is available in version 5.0 or late</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachTransitGatewayToTgw(region, vpcAccountName, vpcName, tgwName, gatewayName, callback)</td>
    <td style="padding:15px">Attaches the Aviatrix Edge VPC to the AWS Transit Gateway.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachTransitGatewayToTgw(tgwName, vpcName, callback)</td>
    <td style="padding:15px">Detaches the Aviatrix Edge VPC to the AWS Transit Gateway.</td>
    <td style="padding:15px">{base_path}/{version}/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
